# latest APK

One of the most annoying things in the mobile app market is developers deciding to abandon support for older Android versions. Instead of shuffling through each and every version on APKmirror and such sites trying to find the latest compatible version of an app for your device, just check available ones in your Android version's folder here. If you cannot find it, look at the older folders as some developers don't lift the bar on the minimum android version

## Getting started

Start by looking at the folder with your android version (or the closest version under it) and find the app. If you are lazy, you could use the search function. If you cannot find it in any of the folders, submit a request in `issues` with the **name of the app** and **your Android version**

all files here are official APKs from APKmirror, APKpure, Aptoide, and fdroid